<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Article;

class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article")
     */
    public function index()
    {

        $entityManager = $this->getDoctrine()->getManager();

        $article = new Article();
        $article->setNom('boli');
        $article->setCodi(2);
        $article->setCategoria('llibreria');
        $article->setStock(22);
        $article->setPreu(2.50);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($article);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return $this->render('article/index.html.twig', [
            'article' => $article->getCodi(),
            'nom' => $article->getNom(),
            'categoria' => $article->getCategoria(),
            'stock' => $article->getStock(),
            'preu' => $article->getPreu()
        ]);


    }


    /**
     * @Route("/article_show/{codi}", name="article_show")
     */
    public function show($codi)
    {
        $article = $this->getDoctrine()
            ->getRepository(Article::class)
            ->find($codi);

        if (!$article) {
            throw $this->createNotFoundException(
                'No product found for codi '.$codi
            );
        }

        return $this->render('article/index.html.twig', [
            'codi' => $article->getCodi(),
            'nom' => $article->getNom()
        ]);
    }


    /**
     * @Route("/article_consulta/{codi}", name="article_consulta")
     */
    public function consulta($codi)
    {
        $article = $this->getDoctrine()
            ->getRepository(Article::class)
            ->find($codi);

        if (!$article) {
            throw $this->createNotFoundException(
                'No product found for codi '.$codi
            );
        }

        return new Response('Check out this great product: '.$article->getNom());

        // or render a template
        // in the template, print things with {{ $article.name }}
        // return $this->render('$article/show.html.twig', ['product' => $article]);
    }


    /**
     * @Route("/article/edit/{codi}")
     */
    public function update($codi)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Article::class)->find($codi);

        if (!$article) {
            throw $this->createNotFoundException(
                'No product found for article '.$codi
            );
        }

        $article->setNom('New product name!');
        $entityManager->flush();

        return $this->redirectToRoute('article_show', [
            'codi' => $article->getCodi()
        ]);
    }

    /**
     * @Route("/article/delete/{codi}")
     */
    public function delete($codi)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Article::class)->find($codi);

        if (!$article) {
            throw $this->createNotFoundException(
                'No product found for article '.$codi
            );
        }

        $entityManager->remove($article);
        $entityManager->flush();

        return $this->redirectToRoute('/article/index.html.twig', [
            'codi' => $article->getCodi()
        ]);
    }



}
